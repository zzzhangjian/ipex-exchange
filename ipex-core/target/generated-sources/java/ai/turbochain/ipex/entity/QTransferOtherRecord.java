package ai.turbochain.ipex.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTransferOtherRecord is a Querydsl query type for TransferOtherRecord
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTransferOtherRecord extends EntityPathBase<TransferOtherRecord> {

    private static final long serialVersionUID = 491081018L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QTransferOtherRecord transferOtherRecord = new QTransferOtherRecord("transferOtherRecord");

    public final NumberPath<java.math.BigDecimal> arrivedAmount = createNumber("arrivedAmount", java.math.BigDecimal.class);

    public final QCoin coin;

    public final DateTimePath<java.util.Date> createTime = createDateTime("createTime", java.util.Date.class);

    public final NumberPath<java.math.BigDecimal> fee = createNumber("fee", java.math.BigDecimal.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> memberIdFrom = createNumber("memberIdFrom", Long.class);

    public final QMember memberTo;

    public final NumberPath<Integer> status = createNumber("status", Integer.class);

    public final NumberPath<java.math.BigDecimal> totalAmount = createNumber("totalAmount", java.math.BigDecimal.class);

    public final NumberPath<Integer> type = createNumber("type", Integer.class);

    public final NumberPath<Long> walletIdFrom = createNumber("walletIdFrom", Long.class);

    public final NumberPath<Long> walletIdTo = createNumber("walletIdTo", Long.class);

    public QTransferOtherRecord(String variable) {
        this(TransferOtherRecord.class, forVariable(variable), INITS);
    }

    public QTransferOtherRecord(Path<? extends TransferOtherRecord> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QTransferOtherRecord(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QTransferOtherRecord(PathMetadata metadata, PathInits inits) {
        this(TransferOtherRecord.class, metadata, inits);
    }

    public QTransferOtherRecord(Class<? extends TransferOtherRecord> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.coin = inits.isInitialized("coin") ? new QCoin(forProperty("coin")) : null;
        this.memberTo = inits.isInitialized("memberTo") ? new QMember(forProperty("memberTo"), inits.get("memberTo")) : null;
    }

}

