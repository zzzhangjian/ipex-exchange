package ai.turbochain.ipex.dao;

import ai.turbochain.ipex.dao.base.BaseDao;
import ai.turbochain.ipex.entity.WebsiteInformation;

/**
 * @author jack
 * @description
 * @date 2020/1/25 18:22
 */
public interface WebsiteInformationDao extends BaseDao<WebsiteInformation> {
}
