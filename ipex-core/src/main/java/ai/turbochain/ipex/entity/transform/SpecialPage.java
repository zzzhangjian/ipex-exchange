package ai.turbochain.ipex.entity.transform;

import lombok.Data;

import java.util.List;

/**
 *
 * @author jack
 * @date 2018年01月15日
 */
@Data
public class SpecialPage<E> {

    private List<E> context;
    private int currentPage;
    private int totalPage;
    private int pageNumber;
    private int totalElement;
}
